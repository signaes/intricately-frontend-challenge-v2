export default function({
  store,
  env: { API_URL, NUMBER_OF_CARDS, PILE_NAME }
}) {
  return store.dispatch('setEnv', { API_URL, NUMBER_OF_CARDS, PILE_NAME })
}
