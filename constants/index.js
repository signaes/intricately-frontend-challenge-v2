export const VALUES = [
  '2',
  'A',
  'K',
  'Q',
  'J',
  '10',
  '9',
  '8',
  '7',
  '6',
  '5',
  '4',
  '3'
]
export const SUITS = ['H', 'D', 'C', 'S']
export const SUITS_NAMES = {
  H: 'hearts',
  D: 'diamonds',
  C: 'clubs',
  S: 'spades'
}
export const VALID_CARD_IDS = (function() {
  const list = []

  for (const value of VALUES) {
    for (const suit of SUITS) {
      list.push(`${value}${suit}`)
    }
  }

  return list
})()

export const errorMessages = {
  card: {
    duplicate: 'Another card already have this value.',
    invalid:
      'This value/suit combination is not valid. This should be a string like "3D" for example.'
  },
  deck: {
    missingCard: 'At least a regular card and a rotation card are required',
    missingRotationCard: 'A rotation card is required'
  }
}
