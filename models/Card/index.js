import Deck from '@/models/Deck'

export default class Card {
  constructor(id) {
    this.id = id
    this.value = id.slice(0, id.length - 1)
    this.suit = id.slice(id.length - 1)
  }

  isValid() {
    return Deck.cardIsValid(this)
  }
}
