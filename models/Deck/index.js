import Combinatorics from 'js-combinatorics'
import { groupBy, union, uniq } from 'lodash'
import { SUITS, VALUES } from './constants'
import { VALID_CARD_IDS } from '@/constants'
import Card from '@/models/Card'

function itemByKey(list, itemKey) {
  return list.find(({ key }) => key === itemKey)
}

function sortCards(cards, suits, values) {
  function suitByKey(suitKey) {
    return itemByKey(suits, suitKey)
  }

  function valueByKey(valueKey) {
    return itemByKey(values, valueKey)
  }

  return cards.sort((a, b) => {
    if (suitByKey(a.suit).value < suitByKey(b.suit).value) {
      return -1
    }

    if (suitByKey(a.suit).value > suitByKey(b.suit).value) {
      return 1
    }

    if (valueByKey(a.value).value < valueByKey(b.value).value) {
      return -1
    }

    if (valueByKey(a.value).value > valueByKey(b.value).value) {
      return 1
    }

    return 0
  })
}

export default class Deck {
  constructor(suits = SUITS, values = VALUES) {
    this.suits = suits
    this.values = values
  }

  static validCards() {
    return VALID_CARD_IDS.map((id) => new Card(id))
  }

  static cardIsValid(card) {
    return VALID_CARD_IDS.includes(card.id)
  }

  static sort(cards) {
    return sortCards(cards, Object.values(SUITS), Object.values(VALUES))
  }

  static sortWithRotationCard(cards, rotationCard) {
    const suitsKeys = Object.values(SUITS).map(({ key }) => key)
    const rotationSuitIndex = suitsKeys.indexOf(rotationCard.suit)
    const suits = suitsKeys
      .slice(rotationSuitIndex)
      .concat(suitsKeys.slice(0, rotationSuitIndex))
      .map((key, idx) => ({ key, value: idx + 1 }))
    const valuesKeys = Object.values(VALUES).map(({ key }) => key)
    const rotationValueIndex = valuesKeys.indexOf(rotationCard.value)
    const values = valuesKeys
      .slice(rotationValueIndex)
      .concat(valuesKeys.slice(0, rotationValueIndex))
      .map((key, idx) => ({ key, value: idx + 1 }))

    return sortCards(cards, suits, values)
  }

  static fullHouseCombinations(cards) {
    if (cards.length < 5) {
      return []
    }

    const cardsByRanksGroups = groupBy(cards, (card) => card.value)
    const cardsByRanks = Object.values(cardsByRanksGroups)
      .map((group) => group.map((card) => card.id))
      .filter((group) => group.length >= 2)

    if (!cardsByRanks.some((group) => group.length >= 3)) {
      return []
    }

    const combinations = (function() {
      let combination
      const combinations = { 3: [], 2: [] }
      const list = []

      cardsByRanks.forEach((group) => {
        if (group.length < 2) {
          return
        }

        const twos = Combinatorics.combination(group, 2)

        while ((combination = twos.next())) {
          combinations[2].push(combination)
        }

        if (group.length < 3) {
          return
        }

        const threes = Combinatorics.combination(group, 3)

        while ((combination = threes.next())) {
          combinations[3].push(combination)
        }
      })

      combinations[3].forEach((three) => {
        combinations[2].forEach((two) => {
          list.push(union(three, two))
        })
      })

      return uniq(list.filter((group) => group.length === 5))
    })()

    return combinations
  }

  static highCard(cards) {
    const sortedCards = Deck.sort(cards)
    const highCard = sortedCards[0]

    return highCard
  }
}
