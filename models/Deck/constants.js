import {
  SUITS as SUITS_KEYS,
  VALUES as VALUES_KEYS,
  SUITS_NAMES
} from '@/constants'

export const SUITS = Object.freeze(
  SUITS_KEYS.reduce((object, suit, idx) => {
    object[SUITS_NAMES[suit]] = {
      key: suit,
      value: idx + 1
    }

    return object
  }, {})
)

export const VALUES = Object.freeze(
  VALUES_KEYS.reduce((object, value, idx) => {
    object[value] = {
      key: value,
      value: idx + 1
    }

    return object
  }, {})
)
