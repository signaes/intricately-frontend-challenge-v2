export const state = () => ({
  deckId: '',
  cards: {},
  rotationCard: {},
  env: {},
  drawn: []
})

export const mutations = {
  setDeck(state, deckId) {
    state.deckId = deckId
  },
  setCards(state, cards) {
    state.cards = cards
  },
  updateCard(state, card) {
    state.cards[card.id] = card

    if (card.rotationCard) {
      state.rotationCard = card
    }
  },
  setEnv(state, env) {
    state.env = env
  }
}

export const actions = {
  setDeck({ commit }, deckId) {
    commit('setDeck', deckId)
  },
  setCards({ commit }, cards) {
    commit('setCards', cards)
  },
  updateCard({ commit }, card) {
    commit('updateCard', card)
  },
  setEnv({ commit }, env) {
    commit('setEnv', env)
  },
  nuxtServerInit({ commit }, { req }) {
    /**
     * Check if is not the /new path and get the deckId if so
     */
    if (req.url.indexOf('deck') !== 1 || req.url === '/deck/new/') {
      return
    }

    const [, deckId] = req.url.replace(/\//g, '').split('deck')

    commit('setDeck', deckId)
  }
}
