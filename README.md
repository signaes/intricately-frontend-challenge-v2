# intricately-frontend-challenge-v2

## Build Setup

``` bash
# setup .env file and docker
$ yarn setup

# serve with docker with hot reload at localhost:3000
$ yarn dev

# test with docker
$ yarn test
```
