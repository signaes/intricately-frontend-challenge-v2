FROM node:13.10.1-alpine3.10

RUN mkdir -p /usr/src/intricately-frontend-challenge-v2
WORKDIR /usr/src/intricately-frontend-challenge-v2

RUN apk update && apk upgrade && apk add python python3 make g++ bash

COPY . /usr/src/intricately-frontend-challenge-v2
RUN yarn

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000
