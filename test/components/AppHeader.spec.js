import { mount, shallowMount } from '@vue/test-utils'
import AppHeader from '@/components/AppHeader.vue'

describe('AppHeader', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(AppHeader)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders what is passed in the slot', () => {
    const wrapper = shallowMount(AppHeader, {
      slots: {
        default: '<div class="title">The title</div>'
      }
    })

    expect(wrapper.findAll('.title').length).toBe(1)
    expect(
      wrapper
        .find('.title')
        .text()
        .trim()
    ).toBe('The title')
  })
})
