import { mount, shallowMount } from '@vue/test-utils'
import AppMain from '@/components/AppMain.vue'

describe('AppMain', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(AppMain)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders what is passed in the slot', () => {
    const wrapper = shallowMount(AppMain, {
      slots: {
        default: '<div class="content">The content</div>'
      }
    })

    expect(wrapper.findAll('.content').length).toBe(1)
    expect(
      wrapper
        .find('.content')
        .text()
        .trim()
    ).toBe('The content')
  })
})
