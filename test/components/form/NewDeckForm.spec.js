import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import NewDeckForm from '@/components/form/NewDeckForm.vue'
import { errorMessages } from '@/constants'

const localVue = createLocalVue()
const NUMBER_OF_CARDS = 10

localVue.use(Vuex)

describe('NewDeckForm', () => {
  let actions, store, state

  beforeEach(() => {
    actions = {
      setCards: jest.fn(),
      updateCard: jest.fn(),
      setDeck: jest.fn()
    }
    state = {
      cards: {
        'card-1': { value: '' },
        'card-2': { value: '' },
        'card-3': { value: '' },
        'card-4': { value: '' },
        'card-5': { value: '' },
        'card-6': { value: '' },
        'card-7': { value: '' },
        'card-8': { value: '' },
        'card-9': { value: '' },
        'card-10': { value: '' },
        'rotation-card': { value: '' }
      }
    }
    store = new Vuex.Store({
      actions,
      state
    })
    process.env = { NUMBER_OF_CARDS }
  })

  test('is a Vue instance', () => {
    const wrapper = mount(NewDeckForm, {
      store,
      localVue,
      propsData: {
        apiUrl: '',
        numberOfCards: 10,
        pileName: 'main'
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders the correct number of inputs', () => {
    const wrapper = mount(NewDeckForm, {
      store,
      localVue,
      propsData: {
        apiUrl: '',
        numberOfCards: NUMBER_OF_CARDS,
        pileName: 'main'
      }
    })

    /**
     * The expected number consists of the number of cards + the rotation card
     */
    const expectedNumber = NUMBER_OF_CARDS + 1

    expect(wrapper.findAll('input').length).toEqual(expectedNumber)
  })

  test('renders a rotation card input', () => {
    const wrapper = mount(NewDeckForm, {
      store,
      localVue,
      propsData: {
        apiUrl: '',
        numberOfCards: NUMBER_OF_CARDS,
        pileName: 'main'
      }
    })

    expect(wrapper.findAll('input#rotation-card').length).toEqual(1)
  })

  describe('when hovering the submit button', () => {
    describe('if some card input does not have a value', () => {
      test('it display an error and the submit button is disabled ', async () => {
        const wrapper = mount(NewDeckForm, {
          store,
          localVue,
          propsData: {
            apiUrl: '',
            numberOfCards: NUMBER_OF_CARDS,
            pileName: 'main'
          }
        })

        wrapper.find('button[type="submit"]').trigger('mouseover')

        await wrapper.vm.$nextTick()

        expect(wrapper.findAll('.errors > p').length).toEqual(1)
        expect(
          wrapper
            .findAll('.errors > p')
            .at(0)
            .text()
        ).toEqual(errorMessages.deck.missingCard)
        expect(
          wrapper.find('button[type="submit"]').attributes('disabled')
        ).toEqual('disabled')
      })
    })

    test('when a regular card has a value but the rotation card is missing, it display an error and the submit button is disabled', async () => {
      state = {
        cards: {
          ...state.cards,
          'card-1': { value: '3D' },
          'rotation-card': { value: '' }
        }
      }
      store = new Vuex.Store({
        actions,
        state
      })
      const wrapper = mount(NewDeckForm, {
        store,
        localVue,
        propsData: {
          apiUrl: '',
          numberOfCards: NUMBER_OF_CARDS,
          pileName: 'main'
        }
      })

      wrapper.find('input:first-child').trigger('blur')
      wrapper.find('button[type="submit"]').trigger('mouseover')

      await wrapper.vm.$nextTick()

      expect(wrapper.findAll('.errors > p').length).toEqual(1)
      expect(
        wrapper
          .findAll('.errors > p')
          .at(0)
          .text()
      ).toEqual(errorMessages.deck.missingRotationCard)
      expect(
        wrapper.find('button[type="submit"]').attributes('disabled')
      ).toEqual('disabled')
    })
  })

  test('when two cards have the same value the submit button is disabled', async () => {
    state = {
      cards: {
        ...state.cards,
        'card-1': { value: '3D' },
        'card-2': { value: '3D' },
        'rotation-card': { value: '' }
      }
    }
    store = new Vuex.Store({
      actions,
      state
    })
    const wrapper = mount(NewDeckForm, {
      store,
      localVue,
      propsData: {
        apiUrl: '',
        numberOfCards: NUMBER_OF_CARDS,
        pileName: 'main'
      }
    })

    wrapper.find('input#card-2').trigger('blur')
    wrapper.find('button[type="submit"]').trigger('mouseover')

    await wrapper.vm.$nextTick()

    expect(
      wrapper.find('button[type="submit"]').attributes('disabled')
    ).toEqual('disabled')
  })
})
