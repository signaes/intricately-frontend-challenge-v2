import { mount, shallowMount } from '@vue/test-utils'
import CardInput from '@/components/form/CardInput.vue'
import { errorMessages } from '@/constants'

describe('CardInput', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: '2D'
      }
    })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders a label if received one via props', () => {
    const id = 'c1'
    const label = 'Card # 1'
    const wrapper = mount(CardInput, {
      propsData: {
        id,
        label
      }
    })

    expect(wrapper.find(`label[for="${id}"]`).exists()).toBe(true)
    expect(
      wrapper
        .find(`label[for="${id}"]`)
        .text()
        .trim()
    ).toEqual(label)
  })

  test('renders what is passed in the slot', () => {
    const wrapper = shallowMount(CardInput, {
      propsData: {
        id: 'card-1'
      },
      slots: {
        default: '<div class="content">The content</div>'
      }
    })

    expect(wrapper.findAll('.content').length).toBe(1)
    expect(
      wrapper
        .find('.content')
        .text()
        .trim()
    ).toBe('The content')
  })

  test('inputed value is transformed to uppercase', async () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1'
      }
    })
    const value = '3d'

    wrapper.find('input').setValue(value)

    await wrapper.vm.$nextTick()

    expect(wrapper.find('input').element.value).toEqual(value.toUpperCase())
    expect(wrapper.vm.value).toEqual(value.toUpperCase())
  })

  test('inputed value is trimmed', async () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1'
      }
    })
    const value = '   3D     '

    wrapper.find('input').setValue(value)

    await wrapper.vm.$nextTick()

    expect(wrapper.find('input').element.value).toEqual(value.trim())
    expect(wrapper.vm.value).toEqual(value.trim())
  })

  test('with an emtpy value is considered valid', async () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'c1'
      }
    })

    wrapper.find('input').setValue('')

    await wrapper.vm.$nextTick()

    expect(wrapper.vm.errors.length).toEqual(0)
    expect(wrapper.find('.errors').exists()).toEqual(false)
  })

  test('display error on invalid card value after blur', async () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1'
      }
    })
    const value = '3x'

    expect(wrapper.text()).not.toContain(errorMessages.card.invalid)

    wrapper.find('input').setValue(value)
    wrapper.find('input').trigger('blur')

    await wrapper.vm.$nextTick()

    expect(wrapper.text()).toContain(errorMessages.card.invalid)
  })

  test('the container have "has-errors" in its class when there are errors', async () => {
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1'
      }
    })
    const value = '3232eqsdasx'

    expect(wrapper.classes()).not.toContain('has-errors')

    wrapper.find('input').setValue(value)
    wrapper.find('input').trigger('blur')

    await wrapper.vm.$nextTick()

    expect(wrapper.classes()).toContain('has-errors')
  })

  test('display error if there is another card with the same value in the cards prop', async () => {
    const value = '3D'
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1',
        cards: { 'card-2': { id: 'card-2', value } }
      }
    })

    expect(wrapper.text()).not.toContain(errorMessages.card.duplicate)

    wrapper.find('input').setValue(value)
    wrapper.find('input').trigger('blur')

    await wrapper.vm.$nextTick()

    expect(wrapper.text()).toContain(errorMessages.card.duplicate)
  })

  test('emits input on change', async () => {
    const value = '3D'
    const wrapper = mount(CardInput, {
      propsData: {
        id: 'card-1'
      }
    })

    wrapper.find('input').setValue(value)

    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().input[0]).toEqual([
      {
        id: 'card-1',
        value,
        isValid: true,
        rotationCard: false
      }
    ])
  })

  describe('if there are errors, emits error on change', () => {
    test('emits invalid card error', async () => {
      const wrapper = mount(CardInput, {
        propsData: {
          id: 'card-1'
        }
      })
      const value = '3x'

      wrapper.find('input').setValue(value)
      wrapper.find('input').trigger('change')

      await wrapper.vm.$nextTick()

      expect(wrapper.emitted().errors[0][0]).toEqual([
        errorMessages.card.invalid
      ])
    })

    test('emits duplicate card error', async () => {
      const value = '3D'
      const wrapper = mount(CardInput, {
        propsData: {
          id: 'card-1',
          cards: { 'card-2': { id: 'card-2', value } }
        }
      })

      wrapper.find('input').setValue(value)
      wrapper.find('input').trigger('change')

      await wrapper.vm.$nextTick()

      expect(wrapper.emitted().errors[0][0]).toEqual([
        errorMessages.card.duplicate
      ])
    })
  })

  describe('emits blur event on blur', () => {
    test('emits on blur', async () => {
      const value = '3D'
      const wrapper = mount(CardInput, {
        propsData: {
          id: 'card-1'
        }
      })

      wrapper.find('input').setValue(value)
      wrapper.find('input').trigger('blur')

      await wrapper.vm.$nextTick()

      expect(Object.keys(wrapper.emitted()).includes('blur')).toBe(true)
    })

    test('the handleBlur method emits blur', () => {
      let eventName = ''
      const $emit = (event) => {
        eventName = event
      }
      CardInput.methods.handleBlur.call({ $emit })

      expect(eventName).toEqual('blur')
    })
  })
})
