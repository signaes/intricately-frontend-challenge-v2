import { VALID_CARD_IDS } from '@/constants'
import { SUITS, VALUES } from '@/models/Deck/constants'
import Deck from '@/models/Deck'
import Card from '@/models/Card'

describe('Deck', () => {
  test('when suits and values are not passed in, it should have default values', () => {
    const deck = new Deck()

    expect(deck.suits).toBe(SUITS)
    expect(deck.values).toBe(VALUES)
  })

  describe('static #validCards()', () => {
    test('return a list of "Card"s with valid id', () => {
      expect(Deck.validCards()).toEqual(
        VALID_CARD_IDS.map((id) => new Card(id))
      )
    })
  })

  describe('static #cardIsValid(card)', () => {
    test('return whether a card has a valid id', () => {
      const invalidIds = ['11D', 'Xyz']

      VALID_CARD_IDS.forEach((id) => {
        expect(Deck.cardIsValid(new Card(id))).toBeTruthy()
      })

      invalidIds.forEach((id) => {
        expect(Deck.cardIsValid(new Card(id))).toBeFalsy()
      })
    })
  })
})
