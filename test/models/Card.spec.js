import Card from '@/models/Card'
import { VALUES, SUITS, VALID_CARD_IDS } from '@/constants'

const validCards = VALID_CARD_IDS.map((id) => new Card(id))

describe('Card', () => {
  test('should be valid only if this is a possible value/suit combination', () => {
    const invalidCards = [new Card('AA'), new Card('ABC')]

    invalidCards.forEach((invalidCard) =>
      expect(invalidCard.isValid()).toBeFalsy()
    )
    validCards.forEach((validCard) => {
      expect(validCard.isValid()).toBeTruthy()
    })
  })

  test('should have value and suit attributes', () => {
    validCards.forEach((validCard) => {
      const { id } = validCard
      const value = id.slice(0, id.length - 1)
      const suit = id.slice(id.length - 1)

      expect(validCard.value).toBe(value)
      expect(validCard.suit).toBe(suit)
      expect(VALUES.includes(validCard.value)).toBeTruthy()
      expect(SUITS.includes(validCard.suit)).toBeTruthy()
    })
  })
})
